printf -v oclock "%d" `expr $(date +%H%M) % 1200`

clock=🕛

 [ ${oclock} -gt 14 ] && clock=🕧
 [ ${oclock} -gt 44 ] && clock=🕐
 [ ${oclock} -gt 114 ] && clock=🕜
 [ ${oclock} -gt 144 ] && clock=🕑
 [ ${oclock} -gt 214 ] && clock=🕝
 [ ${oclock} -gt 244 ] && clock=🕒
 [ ${oclock} -gt 314 ] && clock=🕞
 [ ${oclock} -gt 344 ] && clock=🕓
 [ ${oclock} -gt 414 ] && clock=🕟
 [ ${oclock} -gt 444 ] && clock=🕔
 [ ${oclock} -gt 514 ] && clock=🕠
 [ ${oclock} -gt 544 ] && clock=🕕
 [ ${oclock} -gt 614 ] && clock=🕡
 [ ${oclock} -gt 644 ] && clock=🕖
 [ ${oclock} -gt 714 ] && clock=🕢
 [ ${oclock} -gt 744 ] && clock=🕗
 [ ${oclock} -gt 814 ] && clock=🕣
 [ ${oclock} -gt 844 ] && clock=🕘
 [ ${oclock} -gt 914 ] && clock=🕤
 [ ${oclock} -gt 944 ] && clock=🕙
 [ ${oclock} -gt 1014 ] && clock=🕥
 [ ${oclock} -gt 1044 ] && clock=🕚
 [ ${oclock} -gt 1114 ] && clock=🕦 
 [ ${oclock} -gt 1144 ] && clock=🕛

echo ${clock}
